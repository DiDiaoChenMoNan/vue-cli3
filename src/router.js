import VueRouter from "vue-router";
const routes = [
  {
    path: "/",
    name: "home",
    component: () => import(/* webpackChunkName: "home" */ "./views/Home.vue")
  },
  {
    path: "/about",
    name: "about",
    component: () => import(/* webpackChunkName: "about" */ "./views/About.vue")
  },
  {
    path: "*",
    name: "error_404",
    meta: {
      hideInMenu: true
    },
    component: () => import("@/views/error-page/404.vue")
  }
];

const router = new VueRouter({
  // mode: "history",//该模式会在服务器上刷新变404,还需后端设置
  base: process.env.BASE_URL,
  routes
});

export default router;
